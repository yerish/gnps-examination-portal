<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('student.login.form');
});

//Student Routes

// Authentication Routes Student...
Route::get('login', 'Section\Student\LoginController@showLoginForm')->name('student.login.form');
Route::post('login', 'Section\Student\LoginController@login')->name('student.login.submit');
Route::post('logout', 'Section\Student\LoginController@logout')->name('student.logout');

Route::get('dashboard', 'Section\Student\StudentController@index')->name('student.dashboard');
Route::get('exam/{uuid}', 'Section\Student\StudentController@exam')->name('student.exam');

//Teacher Routes
Route::prefix('teacher')->group(function () {
    Route::get('/', function () {
        return redirect()->route('teacher.login.form');
    });
    Route::get('login', 'Section\Teacher\LoginController@showLoginForm')->name('teacher.login.form');
    Route::post('login', 'Section\Teacher\LoginController@login')->name('teacher.login.submit');
    Route::post('logout', 'Section\Teacher\LoginController@logout')->name('teacher.logout');

    Route::get('dashboard', 'Section\Teacher\TeacherController@index')->name('teacher.dashboard');
    Route::resource('student', 'Section\Teacher\StudentController');
    Route::post('student/class', 'Section\Teacher\StudentController@studentByClass')->name('student.class');
    Route::get('student/class/{class}', 'Section\Teacher\StudentController@filterByClass')->name('class.filter');
    Route::resource('question', 'Section\Teacher\QuestionController');
    Route::post('question/filter', 'Section\Teacher\QuestionController@filter')->name('question.filter');
    Route::get('exam/all', 'Section\Teacher\ExamController@allExam')->name('all.exam');
    Route::post('exam/delete/question', 'Section\Teacher\ExamController@deleteQuestion')->name('exam.delete.question');
    Route::resource('exam', 'Section\Teacher\ExamController');
    Route::resource('result', 'Section\Teacher\ResultController');
    Route::post('result/filter', 'Section\Teacher\ResultController@filter')->name('result.filter');
});

//Admin Routes
Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return redirect()->route('admin.login.form');
    });
    Route::get('login', 'Section\Admin\LoginController@showLoginForm')->name('admin.login.form');
    Route::post('login', 'Section\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('logout', 'Section\Admin\LoginController@logout')->name('admin.logout');

    Route::get('/dashboard', 'Section\Admin\AdminController@index')->name('admin.dashboard');
    Route::resource('/class', 'Section\Admin\ClassController')->names([
        'index' => 'admin.class', 'store' => 'admin.class.store', 'destroy' => 'admin.class.destroy',
        'update' => 'admin.class.update'
    ])->only(['index', 'store', 'destroy', 'update']);
    Route::get('/event', 'Section\Admin\EventController@index')->name('admin.event');
    Route::resource('/teacher', 'Section\Admin\TeacherController')->names([
        'index' => 'admin.teacher', 'store' => 'admin.teacher.store', 'destroy' => 'admin.teacher.destroy',
        'update' => 'admin.teacher.update'
    ])->only(['index', 'store', 'destroy', 'update']);


});

Route::prefix('api')->group(function () {
    Route::get('class', 'AjaxController@classQuestion');
    Route::get('question', 'AjaxController@allQuestion');
    Route::get('{class}/subject', 'AjaxController@subjectQuestion');
    Route::get('{class}/{subject}/chapter', 'AjaxController@chapterQuestion');
    Route::get('{class}/{subject}/{chapter}/topic', 'AjaxController@topicQuestion');
    Route::get('{class}/{subject}/{chapter}/{topic}/level', 'AjaxController@levelQuestion');
    Route::post('questions', 'AjaxController@fetchQuestion');
    Route::get('exam/{uuid}/questions', 'AjaxController@fetchExamQuestion');
    Route::post('exam/submit/questions', 'AjaxController@submitExamQuestions');
});
