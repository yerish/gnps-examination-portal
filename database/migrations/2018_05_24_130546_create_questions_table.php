<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('serial')->unique();
            $table->integer('teacher_id')->unsigned();
            $table->integer('class');
            $table->string('subject');
            $table->string('chapter');
            $table->string('topic');
            $table->string('level');
            $table->longText('question');
            $table->string('upload_question')->nullable();
            $table->integer('option1');
            $table->integer('option2');
            $table->integer('option3');
            $table->integer('option4');
            $table->integer('answer');
            $table->longText('solution');
            $table->string('upload_solution')->nullable();
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
