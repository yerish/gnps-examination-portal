<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Gaurav Abbhi',
            'username' => 'gaurav.abbhi@gmail.com',
            'password' => Hash::make(123456),
        ]);
    }
}
