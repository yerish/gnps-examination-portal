<?php

use App\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
            'serial' => Str::uuid(),
            'creator_id' => '1',
            'name' => 'Gaurav Abbhi',
            'username' => 'gaurav_abbhi',
            'class' => '12',
            'dob' => '1994-03-20',
            'admission_number' => '123456',
            'password' => Hash::make(123456),
        ]);
    }
}
