<?php

use App\Teacher;
use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teacher::create([
            'name' => 'Gaurav Abbhi',
            'username' => 'gaurav.abbhi@gmail.com',
            'password' => Hash::make(123456),
        ]);
    }
}
