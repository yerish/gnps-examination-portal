<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'Gaurav Abbhi',
            'username' => 'gaurav.abbhi@gmail.com',
            'password' => Hash::make(123456),
        ]);
    }
}
