<?php

use Faker\Generator as Faker;

$factory->define(App\Teacher::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->safeEmail,
        'password' => bcrypt(1234),
        'remember_token' => str_random(10),
    ];
});
