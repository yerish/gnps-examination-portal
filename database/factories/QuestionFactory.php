<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'serial' => $faker->uuid,
        'teacher_id' => 1,
        'class' => $faker->numberBetween(1, 12),
        'subject' => $faker->randomElement(['hindi', 'sst', 'science', 'maths', 'english', 'punjabi']),
        'chapter' => $faker->word,
        'topic' => $faker->sentence(),
        'level' => $faker->randomElement([1, 2, 3, 0]),
        'question' => $faker->paragraph(),
        'upload_question' => $faker->imageUrl($width = 640, $height = 480),
        'option1' => $faker->randomElement([$faker->word, $faker->sentence(), $faker->numberBetween(1,1000) ]),
        'option2' => $faker->randomElement([$faker->word, $faker->sentence(), $faker->numberBetween(1,1000) ]),
        'option3' => $faker->randomElement([$faker->word, $faker->sentence(), $faker->numberBetween(1,1000) ]),
        'option4' => $faker->randomElement([$faker->word, $faker->sentence(), $faker->numberBetween(1,1000) ]),
        'answer' => $faker->randomElement([1, 2, 3, 4]),
        'solution' => $faker->randomElement([$faker->word, $faker->paragraph(), $faker->sentence(), null]),
        'upload_solution' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
