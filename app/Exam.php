<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'serial', 'creator_id', 'class', 'name', 'level', 'duration', 'description', 'questions'
    ];
    protected $hidden = [];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'creator_id');
    }

    /**
     * Get the questions associated with the given exam
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany(Question::class, 'exam_questions')->withTimestamps();
    }

    public function getLevelAttribute($value)
    {
        switch ($value) {
            case 1:
                return 'Beginner';
                break;
            case 2:
                return 'Medium';
                break;
            case 3:
                return 'Advance';
                break;
            default:
                return 'All';
        }
    }

    public function result()
    {
        return $this->belongsToMany(Result::class,'results');
    }
}
