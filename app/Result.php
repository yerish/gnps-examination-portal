<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['serial', 'exam_id', 'student_id', 'correct', 'incorrect'];

    public function student()
    {
      return  $this->belongsTo(Student::class,'student_id','id');
    }

    public function exams()
    {
      return  $this->hasMany(Exam::class);
    }
}
