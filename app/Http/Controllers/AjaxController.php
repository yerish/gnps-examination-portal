<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Question;
use App\Result;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AjaxController extends Controller
{
    public function allQuestion()
    {
        return Question::all();
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function classQuestion()
    {
        /** @noinspection PhpParamsInspection */
        return response([
            'classes' => Question::all()->unique('class')->sortBy('class')->pluck('class')
        ]);
    }

    public function subjectQuestion($class)
    {
        /** @noinspection PhpParamsInspection */
        return response([
            'subjects' => Question::where('class', $class)->pluck('subject')->unique()->sort()->flatten()
        ]);
    }

    public function chapterQuestion($class, $subject)
    {
        /** @noinspection PhpParamsInspection */
        return response([
            'chapters' => Question::where(['class' => $class, 'subject' => $subject])->pluck('chapter')->unique()->sort()->flatten()
        ]);
    }

    public function topicQuestion($class, $subject, $chapter)
    {
        /** @noinspection PhpParamsInspection */
        return response([
            'topics' => Question::where(['class' => $class, 'subject' => $subject, 'chapter' => $chapter])->pluck('topic')->unique()->sort()->flatten()
        ]);
    }

    public function levelQuestion($class, $subject, $chapter, $topic)
    {
        /** @noinspection PhpParamsInspection */
        return response([
            'levels' => Question::where(['class' => $class, 'subject' => $subject, 'chapter' => $chapter, 'topic' => $topic])->pluck('level')->unique()->sort()->flatten()
        ]);
    }

    public function fetchQuestion(Request $request)
    {
        switch ($request->level) {
            case 'Beginner':
                $level = 1;
                break;
            case 'Medium':
                $level = 2;
                break;
            case 'Advance':
                $level = 3;
                break;
            default:
                return 0;
        }
        /** @noinspection PhpParamsInspection */
        return response([
            'questions' => Question::with('teacher')->where([
                'class' => $request->class,
                'subject' => $request->subject,
                'chapter' => $request->chapter,
                'topic' => $request->topic,
                'level' => $level,
            ])->get()
        ]);
    }

    public function fetchExamQuestion($uuid)
    {
        $exam_id = Exam::where('serial', $uuid)->pluck('id');
        $questions = Exam::with('questions')->find($exam_id)->first();
        /** @noinspection PhpParamsInspection */
        return response([
            'exam' => $questions
        ]);
    }

    public function submitExamQuestions(Request $request)
    {
        $result = new Result;
        $result->serial = Str::uuid();
        $result->exam_id = $request->exam_id;
        $result->student_id = auth()->id();
        $answers = $request->answers;
        if (collect($answers)->count() === 0) return 0;
        foreach ($answers as $question_id => $value) {
            $x[] = Question::where(['serial' => json_decode($question_id), 'answer' => $value])->pluck('serial')->first();
        }
        $result->correct = collect($x)->reject(function ($value, $key) {
            return $value == null;
        })->count();
        $result->incorrect = collect($answers)->count() - $result->correct;
        $result->save();
        return 0;
    }
}
