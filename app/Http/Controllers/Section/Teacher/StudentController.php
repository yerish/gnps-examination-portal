<?php

namespace App\Http\Controllers\Section\Teacher;

use App\Http\Controllers\Controller;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    /**
     * StudentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = DB::table('classes')->get()->sortBy('class');
        return view('sections.teacher.student.index',compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Student::create([
            'serial' => Str::uuid(),
            'creator_id' => auth('teacher')->id(),
            'name' => $request->name,
            'username' => snake_case($request->name),
            'class' => $request->class,
            'dob' => $this->date($request->dob),
            'admission_number' => $request->admission_number,
            'password' =>  Hash::make($request->admission_number),
        ]);

        return back()->with('status', 'Student added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Student::find($id)->update([
            'creators_id' => auth('teacher')->id(),
            'name' => $request->name,
            'username' => snake_case($request->name),
            'class' => $request->class,
            'dob' => $this->date($request->dob),
            'admission_number' => $request->admission_number,
            'password' =>  Hash::make($request->admission_number),
        ]);

        return back()->with('status','Update Successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);
        return back()->with('status', 'Student deleted');
    }

    public function studentByClass(Request $request)
    {
        $class = $request->class;
        return redirect()->route('class.filter', compact('class'));
    }

    public function filterByClass($class)
    {
        $students = Student::where('class', $class)->get();
        $classes = DB::table('classes')->get()->sortBy('class');
        return view('sections.teacher.student.filter', compact('students','classes'));
    }

    /**
     * @param Request $request
     * @return Carbon
     */
    public function date($date)
    {
        return Carbon::createFromFormat('d/m/Y', $date);
    }
}
