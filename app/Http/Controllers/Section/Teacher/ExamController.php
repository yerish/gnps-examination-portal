<?php

namespace App\Http\Controllers\Section\Teacher;

use App\Exam;
use App\Http\Controllers\Controller;
use App\Question;
use App\Result;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ExamController extends Controller
{
    /**
     * ExamController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sections.teacher.exam.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::latest()->get();
        $classes = DB::table('classes')->get()->sortBy('class');
        return view('sections.teacher.exam.create', compact('questions', 'classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'class' => 'required',
            'level' => 'required',
            'duration' => 'required',
            'description' => 'required',
            'question' => 'required'
        ]);
        $questions = $request->question;
        $exam = Exam::create([
            'serial' => Str::uuid(),
            'creator_id' => auth('teacher')->id(),
            'name' => $request->name,
            'class' => $request->class,
            'level' => $request->level,
            'duration' => $request->duration,
            'description' => $request->description,
        ]);
        foreach ($questions as $question) {
            $question = Question::where('serial', $question)->first();
            $exam->questions()->attach($question->id);
        }
        return back()->with('status', 'Exam created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::find($id);
        $questions = collect($exam->questions);
        return view('sections.teacher.exam.show', compact('questions', 'exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('exam_questions')->where('exam_id', $id)->delete();
        Exam::find($id)->delete();
        return back()->with('status', 'Exam Deleted Successfully');

    }

    public function allExam()
    {
        $exams = Exam::latest()->get();
        return view('sections.teacher.exam.all', compact('exams'));
    }

    public function deleteQuestion(Request $request)
    {
        DB::table('exam_questions')->where(['exam_id' => $request->exam_id, 'question_id' => $request->question_id])->delete();
        return back()->with('status', 'Question Deleted Successfully');
    }

    public function result()
    {
        $result = Result::all();
//        $student = Student::find($result->correct);
        return $result;
    }
}
