<?php

namespace App\Http\Controllers\Section\Teacher;

use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class QuestionController extends Controller
{
    /**
     * QuestionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:teacher');
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_questions = Question::all();
        $questions = Question::where('teacher_id', auth()->id())->latest('updated_at')->get();
        $classes = $this->fetchClasses();
        return view('sections.teacher.question.index', compact('questions', 'all_questions','classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'class' => 'required', 'subject' => 'required',
            'chapter' => 'required', 'topic' => 'required',
            'level' => 'required', 'option1' => 'required',
            'option2' => 'required', 'option3' => 'required',
            'option4' => 'required', 'answer' => 'required',
        ]);
        $upload_question_path = '';
        $upload_solution_path = '';
        if ($request->upload_question) {
            $upload_question_path = $request->file('upload_question')->store("public/class/$request->class/$request->subject/questions");
        }
        if ($request->upload_solution) {
            $upload_solution_path = $request->file('upload_solution')->store("public/class/$request->class/$request->subject/solution");
        }
        Question::create([
            'serial' => Str::uuid(), 'teacher_id' => auth('teacher')->id(),
            'class' => $request->class, 'subject' => $request->subject,
            'chapter' => $request->chapter, 'topic' => $request->topic,
            'level' => $request->level, 'question' => $request->question,
            'upload_question' => $upload_question_path ? $upload_question_path : null, 'option1' => $request->option1,
            'option2' => $request->option2, 'option3' => $request->option3,
            'option4' => $request->option4, 'answer' => $request->answer,
            'solution' => $request->solution, 'upload_solution' => $upload_solution_path ? $upload_solution_path : null
        ]);
        return redirect()->route('question.index')->with('status', "Question added successfully" );
    }

    /**
     * Display the specified resource.
     *
     * @param $serial
     * @return \Illuminate\Http\Response
     */
    public function show($serial)
    {
        return Question::where('serial',$serial)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::where('serial',$id)->get()->first();
        $upload_question_path = '';
        $upload_solution_path = '';

        if ($question->upload_question){
            $upload_question_path =  $question->upload_question;
        }

        if ($question->upload_solution){
            $upload_solution_path =  $question->upload_solution;
        }


        if ($request->upload_question) {
            //update the path and delete previous uploaded question image if new is set
            // if no question uploaded remain it as it was
            if ($question->upload_question !== null){
                \Storage::delete($question->upload_question);
            }
            $upload_question_path = $request->file('upload_question')->store("public/class/$request->class/$request->subject/questions");
        }
        if ($request->upload_solution) {
            if ($question->upload_solution !== null) {
                \Storage::delete($question->upload_solution);
            }
            $upload_solution_path = $request->file('upload_solution')->store("public/class/$request->class/$request->subject/solution");
        }
        Question::where('serial',$id)->update([
            'teacher_id' => auth('teacher')->id(),
            'class' => $request->class, 'subject' => $request->subject,
            'chapter' => $request->chapter, 'topic' => $request->topic,
            'level' => $request->level, 'question' => $request->question,
            'upload_question' => $upload_question_path ? $upload_question_path : null, 'option1' => $request->option1,
            'option2' => $request->option2, 'option3' => $request->option3,
            'option4' => $request->option4, 'answer' => $request->answer,
            'solution' => $request->solution, 'upload_solution' => $upload_solution_path ? $upload_solution_path : null
        ]);
        return redirect()->route('question.index')->with('status', "Question Updated Successfully with id :- $id" );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question= Question::where('serial',$id)->get()->first();
        if ($question->upload_question){
            \Storage::delete($question->upload_question);
        }
        if ($question->upload_solution){
            \Storage::delete($question->upload_solution);
        }
        $question->delete();
        return back()->with('status', "Question deleted successfully with id :- $id" );
    }

    public function filter(Request $request)
    {
        $classes = $this->fetchClasses();
        switch ($request->level) {
            case 'Beginner':
                $level = 1;
                break;
            case 'Medium':
                $level = 2;
                break;
            case 'Advance':
                $level = 3;
                break;
            default:
                $level = 0;
        }
        $questions = Question::with('teacher')->where(['class' => $request->class, 'subject' => $request->subject, 'chapter' => $request->chapter, 'topic' => $request->topic, 'level' => $level])->latest('updated_at')->get();
        return view('sections.teacher.question.filter', compact('questions','classes'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function fetchClasses()
    {
        $classes = DB::table('classes')->get()->sortBy('class');
        return $classes;
    }
}
