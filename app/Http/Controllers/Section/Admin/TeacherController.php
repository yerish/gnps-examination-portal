<?php

namespace App\Http\Controllers\Section\Admin;

use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view('sections.admin.teacher.index',compact('teachers'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Teacher::create([
           'name' => $request->teacher_name,
           'username' => $request->username,
           'password' => bcrypt($request->password),
       ]);

       return back()->with('status', "Teacher $request->teacher_name added successfully");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       Teacher::find($id)->update([
           'name' => $request->name,
           'username' => $request->username,
           'password' => bcrypt($request->password),
       ]);
       return back()->with('status','Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Teacher::find($id)->delete();
      return back()->with('status','Teacher deleted successfully');
    }
}
