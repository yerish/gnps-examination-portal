<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Question extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['serial', 'class', 'teacher_id', 'subject', 'chapter', 'topic', 'level', 'question', 'upload_question', 'option1', 'option2', 'answer', 'option3', 'option4', 'solution', 'upload_solution'];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class,'teacher_id');
    }

    public function getLevelAttribute($value)
    {
        switch ($value) {
            case 1:
                return 'Beginner';
                break;
            case 2:
                return 'Medium';
                break;
            case 3:
                return 'Advance';
                break;
            default:
                return 'All';
        }
    }

    public function getUploadQuestionAttribute($value)
    {
        if($value != null){
            return Storage::url($value);
        }else {
            return null;
        }
    }

    /**
     * Get the exams associated with the given questions
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function exam()
    {
        return $this->belongsToMany(Exam::class,'exam_questions')->withTimestamps();
    }
}
