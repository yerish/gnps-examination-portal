<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeacherTest extends TestCase
{
    use RefreshDatabase;

    protected $teacher;
    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        $this->teacher = factory('App\Teacher')->create();
    }


    /** @test */
    public function it_can_redirects_to_login_page()
    {

        $this->get(route('teacher.login.form'))->assertSee('Teacher\'s Login Form');
    }

    /** @test */
    public function login_user_with_correct_password_get_redirects_to_dashboard()
    {
        $this->from('teacher/login')->post(route('teacher.login.submit'), [
            'email' => $this->teacher->email,
            'password' => "1234"
        ])->assertRedirect(route('teacher.dashboard'));

        $this->get(route('teacher.dashboard'))->assertSee($this->teacher->name);
    }

    /** @test */
    public function login_user_with_wrong_password_redirects_to_same_page_with_errors()
    {
        $this->from(route('teacher.login.form'))->post(route('teacher.login.submit'), [
            'email' => $this->teacher->email,
            'password' => "wrongpassword"
        ])->assertRedirect(route('teacher.login.form'))
            ->assertSessionHasErrors([
                'email' => 'These credentials do not match our records.'
            ]);
    }

    /** @test */
    public function user_logout_and_redirect_to_teacher_login_page()
    {
        $this->be($this->teacher);
        $this->from(route('teacher.dashboard'))
            ->post(route('teacher.logout'))
            ->assertRedirect(route('teacher.login.form'));
    }
}
