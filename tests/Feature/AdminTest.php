<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    protected $admin;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        $this->admin = factory('App\Admin')->create();
    }


    /** @test */
    public function it_can_redirects_to_login_page()
    {
        $this->get('admin/login')->assertSee('Admin Login');
    }

    /** @test */
    public function login_user_with_correct_password_get_redirects_to_admin_dashboard()
    {
        $this->from('admin/login')->post(route('admin.login.submit'), [
            'username' => $this->admin->username,
            'password' => "1234"
        ])->assertRedirect(url('admin/dashboard'));

        $this->get(url('admin/dashboard'))->assertSee($this->admin->name);
    }

    /** @test */
    public function login_user_with_wrong_password_redirects_to_same_page_with_errors()
    {
        $this->from('admin/login')->post(route('admin.login.submit'), [
            'username' => $this->admin->username,
            'password' => "wrongpassword"
        ])->assertRedirect('admin/login')
            ->assertSessionHasErrors([
                'username' => 'These credentials do not match our records.'
            ]);
    }

    /** @test */
    public function user_logout_and_redirect_to_admin_login_page()
    {
        $this->be($this->admin);
        $this->from(route('admin.dashboard'))
            ->post(route('admin.logout'))
            ->assertRedirect('admin/login');
    }
}
