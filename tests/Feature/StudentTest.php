<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    protected $student;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        $this->student = factory('App\User')->create();
    }


    /** @test */
    public function it_can_redirects_to_login_page()
    {
        $this->get('/')->assertRedirect('/login');
        $this->get('/login')->assertSee('Student Login Form');
    }

    /** @test */
    public function login_user_with_correct_password_get_redirects_to_home()
    {
        $this->from('/login')->post(route('student.login.submit'), [
            'email' => $this->student->email,
            'password' => "1234"
        ])->assertRedirect(url('/dashboard'));

        $this->get(url('/dashboard'))->assertSee($this->student->name);
    }

    /** @test */
    public function login_user_with_wrong_password_redirects_to_same_page_with_errors()
    {
        $this->from('/login')->post(route('student.login.submit'), [
            'email' => $this->student->email,
            'password' => "wrongpassword"
        ])->assertRedirect('/login')
            ->assertSessionHasErrors([
                'email' => 'These credentials do not match our records.'
            ]);
    }

    /** @test */
    public function user_logout_and_redirect_to_student_login_page()
    {
        $this->be($this->student);
        $this->from(route('student.dashboard'))
            ->post(route('student.logout'))
            ->assertRedirect('/login');
    }
}
