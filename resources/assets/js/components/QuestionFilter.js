import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import uuid from 'uuid';

export default class QuestionFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'classes': [],
            'subject': [],
            'chapter': [],
            'topic': [],
            'level': [],
            'questions': [],
            'selectedClass': '',
            'selectedSubject': '',
            'selectedChapter': '',
            'selectedTopic': '',
            'selectedLevel': '',
            'disabledClass': false,
            'disabledSubject': true,
            'disabledChapter': true,
            'disabledTopic': true,
            'disabledLevel': true,
            'disabledViewQuestionButton': true,
        };
        this.fetchSubject = this.fetchSubject.bind(this);
        this.fetchChapter = this.fetchChapter.bind(this);
        this.fetchTopic = this.fetchTopic.bind(this);
        this.fetchLevel = this.fetchLevel.bind(this);
        this.fetchQuestions = this.fetchQuestions.bind(this);
    }

    componentDidMount() {
        this.fetchClass();
    }

    fetchClass() {
        axios.get("/api/class")
            .then(response => {
                this.setState((prevState) => {
                    return {classes: response.data.classes}
                });
            }).catch((error) => console.log(error));
    }

    fetchSubject(event) {
        let selectedClass = event.target.value;
        if (selectedClass !== "Choose") {
            axios.get(`/api/${selectedClass}/subject`)
                .then(response => {
                    this.setState((prevState) => {
                        return {
                            subject: response.data.subjects,
                            'chapter': [], 'topic': [], 'level': [],
                            selectedClass,
                            selectedSubject: '',
                            selectedChapter: '',
                            selectedTopic: '',
                            selectedLevel: ''
                        }
                    });
                }).catch((error) => console.log(error));
        }
    }

    fetchChapter(event) {
        let selectedSubject = event.target.value;
        if (selectedSubject !== 'Choose') {
            axios.get(`/api/${this.state.selectedClass}/${selectedSubject}/chapter`)
                .then(response => {
                    this.setState((prevState) => {
                        return {
                            chapter: response.data.chapters,
                            'topic': [], 'level': [],
                            selectedSubject,
                            selectedChapter: '',
                            selectedTopic: '',
                            selectedLevel: ''
                        }
                    });
                }).catch((error) => console.log(error));
        }
    }

    fetchTopic(event) {
        let selectedChapter = event.target.value;
        if (selectedChapter !== 'Choose') {
            axios.get(`/api/${this.state.selectedClass}/${this.state.selectedSubject}/${selectedChapter}/topic`)
                .then(response => {
                    this.setState((prevState) => {
                        return {
                            topic: response.data.topics,
                            level: [],
                            selectedChapter,
                            selectedTopic: '',
                            selectedLevel: ''
                        }
                    });
                }).catch((error) => console.log(error));
        }
    }

    fetchLevel(event) {
        let selectedTopic = event.target.value;
        if (selectedTopic !== 'Choose') {
            axios.get(`/api/${this.state.selectedClass}/${this.state.selectedSubject}/${this.state.selectedChapter}/${selectedTopic}/level`)
                .then(response => {
                    this.setState((prevState) => {
                        return {
                            level: response.data.levels,
                            selectedTopic,
                            selectedLevel: ''
                        }
                    });
                }).catch((error) => console.log(error));
        }
    }

    fetchQuestions(event) {
        let selectedLevel = event.target.value;
        if (selectedLevel !== 'Choose') {
            this.setState((prevState) => {
                return {selectedLevel, disabledViewQuestionButton: false}
            });
        }
    }

    render() {
        return (
            <div className="w-full max-w-sm mx-auto mt-4">
                <div className="bg-red text-lg font-bold text-white border shadow rounded py-1 pl-2">View Question</div>
                <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
                      action={`${process.env.MIX_APP_URL}/teacher/question/filter`}
                      method="post">
                    <input type="hidden" name="_token"
                           value={document.head.querySelector('meta[name="csrf-token"]').content}/>
                    <SelectClass handleSelectedClass={this.fetchSubject} classes={this.state.classes}
                                 selectedClass={this.state.selectedClass} disableClass={this.state.disabledClass}/>
                    <SelectSubject handleSelectedSubject={this.fetchChapter} subjects={this.state.subject}
                                   selectedSubject={this.state.selectedSubject}
                                   disableSubject={this.state.disabledSubject}/>
                    <SelectChapter handleSelectedChapter={this.fetchTopic} chapters={this.state.chapter}
                                   selectedChapter={this.state.selectedChapter}
                                   disableChapter={this.state.disabledChapter}/>
                    <SelectTopic handleSelectedTopic={this.fetchLevel} topics={this.state.topic}
                                 selectedTopic={this.state.selectedTopic} disableTopic={this.state.disabledTopic}/>
                    <SelectLevel handleLevel={this.fetchQuestions} levels={this.state.level}
                                 selectedLevel={this.state.selectedLevel} disableLevel={this.state.disabledLevel}/>
                    <RenderInputs classValue={this.state.selectedClass} subjectValue={this.state.selectedSubject}
                                  chapterValue={this.state.selectedChapter} topicValue={this.state.selectedTopic}
                                  levelValue={this.state.selectedLevel}/>
                    <SubmitButton handleSubmitButton={this.renderQuestions}
                                  disableSubmitButton={this.state.disabledViewQuestionButton}/>
                    <CreateQuestionModalButton/>
                </form>
            </div>
        );
    }
}
;

class SelectClass extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selectedClass === '' ? true : false;
    }

    render() {
        return (
            <div className="form-group row">
                <label htmlFor="view-class" className="col-md-3 col-form-label text-lg font-bold">Class</label>
                <div className="col-md-9">
                    <select id="view-class" name="class"
                            className="appearance-none border rounded py-2 px-3 w-75 text-grey-darker capitalize custom-select"
                            onChange={(e) => this.props.handleSelectedClass(e)}>
                        <option>Choose</option>
                        {
                            this.props.classes.length > 0 ? this.props.classes.map(x => <option key={uuid()}
                                                                                                value={x}>{x} Class</option>) :
                                <option>Loading.....</option>
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class SelectSubject extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selectedSubject === '' ? true : false;
    }

    render() {
        return (
            <div className="form-group row">
                <label htmlFor="view-subject" className="col-md-3 col-form-label text-lg font-bold">Subject</label>
                <div className="col-md-9">
                    <select id="view-subject" name="subject"
                            className="appearance-none border rounded py-2 px-3 w-75 text-grey-darker  capitalize custom-select "
                            onChange={(e) => this.props.handleSelectedSubject(e)}>
                        <option> Choose</option>
                        {
                            this.props.subjects.length > 0 ? this.props.subjects.map(x => <option key={uuid()}
                                                                                                  value={x}>{x}</option>) : ''
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class SelectChapter extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selectedChapter === '' ? true : false;
    }

    render() {
        return (
            <div className="form-group row">
                <label htmlFor="view-chapter" className="col-md-3 col-form-label text-lg font-bold">Chapter</label>
                <div className="col-md-9">
                    <select id="view-chapter" name="chapter"
                            className="appearance-none border rounded py-2 px-3 w-75 text-grey-darker  capitalize custom-select "
                            onChange={(e) => this.props.handleSelectedChapter(e)}>
                        <option>Choose</option>
                        {
                            this.props.chapters.length > 0 ? this.props.chapters.map(x => <option key={uuid()}
                                                                                                  value={x}>{x}</option>) : ''
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class SelectTopic extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selectedTopic === '' ? true : false;
    }

    render() {
        return (
            <div className="form-group row">
                <label htmlFor="view-topic" className="col-md-3 col-form-label text-lg font-bold">Topic</label>
                <div className="col-md-9">
                    <select id="view-topic" name="topic"
                            className="appearance-none border rounded py-2 px-3 w-75 text-grey-darker  capitalize custom-select "
                            onChange={(e) => this.props.handleSelectedTopic(e)}>
                        <option> Choose</option>
                        {
                            this.props.topics.length > 0 ? this.props.topics.map(x => <option key={uuid()}
                                                                                              value={x}>{x}</option>) : ''
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class SelectLevel extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.selectedLevel === '' ? true : false;
    }

    render() {
        return (
            <div className="form-group row">
                <div className="col-3">
                    <label className="col-form-label text-lg font-bold" htmlFor="view-level">Level</label>
                </div>
                <div className="col-md-9">
                    <select id="view-level" name="level"
                            className="appearance-none border rounded py-2 px-3 w-75 text-grey-darker capitalize custom-select"
                            onChange={(e) => this.props.handleLevel(e)}>
                        <option> Choose</option>
                        {
                            this.props.levels.length > 0 ? this.props.levels.map(x => <option key={uuid()}
                                                                                              value={x}>{x}</option>) : ''
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class RenderInputs extends Component {
    render() {
        return (
            <div>
                <input type="hidden" name="class" value={this.props.classValue}/>
                <input type="hidden" name="subject" value={this.props.subjectValue}/>
                <input type="hidden" name="chapter" value={this.props.chapterValue}/>
                <input type="hidden" name="topic" value={this.props.topicValue}/>
                <input type="hidden" name="level" value={this.props.levelValue}/>
            </div>
        )
    };
};

class SubmitButton extends Component {
    render() {
        return (
            <div className="flex items-center justify-between">
                <button
                    className={`form-group btn btn-danger text-white font-bold py-2 px-4 mt-2 mx-auto rounded col-12 ${this.props.disableSubmitButton ? "hidden" : ""}`}
                    type="submit" disabled={this.props.disableSubmitButton}>
                    <span className="text-lg">View Question</span>
                </button>
            </div>
        );
    }
}

class CreateQuestionModalButton extends Component {
    render() {
        return (
            <div className="flex items-center justify-between">
                <button id="create-question-modal-button" data-toggle="modal"
                        data-target="#create-question-modal"
                        className="form-group btn btn-outline-primary text-blue hover:text-white font-bold py-2 px-4 mt-2 mx-auto rounded col-12"
                        type="button">
                    <span className="text-lg">Create Question</span>
                </button>
            </div>
        );
    }
}

if (document.getElementById('view-question')) {
    ReactDOM.render(<QuestionFilter/>, document.getElementById('view-question'));
}
