import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment'
import Countdown from 'react-countdown-moment'
import axios from 'axios';
import collect from 'collect.js';
import _ from 'lodash';

export default class Exam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'active_question_id': '',
            'active_question_number': 1,
            'answers': {},
            'duration': '',
            'exam_id': '',
            'next': true,
            'question_state': {},
            'questions': [],
            'subject': [],
            'studentName': props.studentName
        };
        this.makeQuestionActive = this.makeQuestionActive.bind(this);
        this.saveAndNext = this.saveAndNext.bind(this);
        this.selectAnswer = this.selectAnswer.bind(this);
        this.markForReview = this.markForReview.bind(this);
        this.clearResponse = this.clearResponse.bind(this);
        this.endTime = this.endTime.bind(this);
        this.submit = this.submit.bind(this);
    };

    componentDidMount() {
        this.fetchExamQuestions();
    }

    submit() {
        axios.post('/api/exam/submit/questions', {
            answers: this.state.answers,
            exam_id: this.state.exam_id,
        })
            .then(function (response) {
                console.log(response);
                axios.post('/logout');
                window.location = `${process.env.MIX_APP_URL}/`;
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    endTime() {
        if (this.state.duration != parseInt("0")) {
            setTimeout(this.submit, this.state.duration * 1000 * 60)
        }
        return moment().add(this.state.duration, 'minutes');
    }

    fetchExamQuestions() {
        axios.get(`/api${window.location.pathname}/questions`)
            .then(response => {
                let activeQuestionId = collect(response.data.exam.questions).pluck('serial').first();
                this.setState((prevState) => {
                    return {
                        questions: response.data.exam.questions,
                        duration: response.data.exam.duration,
                        active_question_id: activeQuestionId,
                        question_state: {
                            ...this.state.question_state,
                            [`"${activeQuestionId}"`]: "bg-red text-white "
                        },
                        exam_id: response.data.exam.serial
                    }
                });
            }).catch((error) => console.log(error));
    }

    makeQuestionActive(event) {
        event.persist();
        let activeQuestionId = event.target.value;
        let activeQuestionNumber = event.target.innerText;
        if (this.state.questions.length === parseInt(activeQuestionNumber)) {
            this.setState((prevState) => {
                return {
                    next: false
                }
            });
        } else {
            this.setState((prevState) => {
                return {
                    next: true
                }
            });
        }
        if (_.has(this.state.answers, `"${activeQuestionId}"`) && this.state.answers[`"${activeQuestionId}"`]) {
            this.setState((prevState) => {
                return {
                    active_question_id: activeQuestionId,
                    active_question_number: activeQuestionNumber,
                    question_state: {
                        ...this.state.question_state,
                        [`"${activeQuestionId}"`]: "bg-green text-white "
                    },
                }
            });
        } else {
            this.setState((prevState) => {
                return {
                    active_question_id: activeQuestionId,
                    active_question_number: activeQuestionNumber,
                    question_state: {
                        ...this.state.question_state,
                        [`"${activeQuestionId}"`]: "bg-red text-white "
                    },
                }
            });
        }
    }

    saveAndNext() {
        let questions = collect(this.state.questions).pluck('serial');
        let activeQuestionId = this.state.active_question_id;
        let currentIndex = questions.items.indexOf(activeQuestionId);
        let nextQuestionId = questions.items[++currentIndex];

        /*
        Check for this active question option is selected or not
        if selected then advance the question and make its index state in right side to green
        else advance the question and make its index state in right side to red
        */
        if (_.has(this.state.answers, `"${activeQuestionId}"`) && this.state.answers[`"${activeQuestionId}"`]) {
            this.actionNextQuestion(nextQuestionId, currentIndex, activeQuestionId, "bg-green text-white ");
        } else {
            if (this.state.questions.length !== parseInt(this.state.active_question_number)) {
                this.skipAnswerConfirmation(nextQuestionId, currentIndex, activeQuestionId);
            } else {
                alert("Please select your option for this question before saving it \n or submit your answers using submit button.");
            }
        }


    }

    skipAnswerConfirmation(nextQuestionId, currentIndex, activeQuestionId) {
        let skipAnswer = confirm("You haven't Selected any option for this question \n Want to skip instead. ");
        if (skipAnswer) {
            this.actionNextQuestion(nextQuestionId, currentIndex, activeQuestionId, "bg-red text-white ");
        }
    }

    selectAnswer(event) {
        let question_id = this.state.active_question_id;
        let selected_option = event.target.value;
        this.setState((prevState) => {
            return {
                answers: {...this.state.answers, [`"${question_id}"`]: selected_option}
            }
        });
    }

    markForReview() {
        let activeQuestionId = this.state.active_question_id;
        let questions = collect(this.state.questions).pluck('serial');
        let currentIndex = questions.items.indexOf(activeQuestionId);
        let nextQuestionId = questions.items[++currentIndex];
        this.actionNextQuestion(nextQuestionId, currentIndex, activeQuestionId, "bg-blue text-white ");
    }

    actionNextQuestion(nextQuestionId, currentIndex, activeQuestionId, questionState) {
        if (nextQuestionId !== undefined) {
            this.setState((prevState) => {
                return {
                    active_question_id: nextQuestionId,
                    active_question_number: ++currentIndex,
                    question_state: {
                        ...this.state.question_state,
                        [`"${activeQuestionId}"`]: questionState
                    },
                    next: true
                }
            });
        } else {
            this.setState((prevState) => {
                return {
                    question_state: {...this.state.question_state, [`"${activeQuestionId}"`]: questionState},
                    next: false
                }
            });
        }
    }

    clearResponse() {
        this.setState((prevState) => {
            let activeQuestionId = [`"${this.state.active_question_id}"`];
            return {
                answers: {...this.state.answers, [activeQuestionId]: null}
            }
        });
    }


    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row">
                        <ExamHeader studentName={this.state.studentName} handleEndTime={this.endTime}
                                    duration={this.state.duration}/>
                        <div className="col-md-9 m-0 p-0">
                            <ActiveQuestion
                                question={collect(this.state.questions).firstWhere('serial', this.state.active_question_id) !== null ?
                                    collect(this.state.questions).firstWhere('serial', this.state.active_question_id) : ''}
                                optionAnswers={this.state.answers}
                                handleSelectAnswer={this.selectAnswer}
                                activeQuestion={this.state.active_question_number}/>
                            <ButtonGroup next={this.state.next} handleSaveAndNext={this.saveAndNext}
                                         handleMarkForReview={this.markForReview}
                                         handleClearResponse={this.clearResponse}
                            />
                        </div>
                        <div className="col-md-3 m-0 p-0">
                            <RightSide questions={collect(this.state.questions).pluck('serial')}
                                       handleMakeQuestionActive={this.makeQuestionActive}
                                       questionState={this.state.question_state}
                                       answeredQuestions={this.state.answers}
                                       handleSubmit={this.submit}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class ExamHeader extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.duration == 0 ? true : false;
    }

    render() {
        return (
            <div className="col-md-12 m-0 p-0">
                <div className="flex bg-light">
                    <div className="pl-2">
                        <span className="font-bold">Welcome, {this.props.studentName} </span>
                    </div>
                    <div className="ml-auto" id="#">
                        <span id="#" className="font-bold">Time Left - </span>
                        <span id="#" className="font-bold text-sans pr-2">
                        <Countdown endDate={this.props.handleEndTime()}/>
                    </span>
                    </div>
                </div>
            </div>
        )
    }
}

class ActiveQuestion extends Component {
    activeQuestion() {
        return `"${this.props.question.serial}"`;
    }

    questionAttemptedBefore() {
        return _.has(this.props.optionAnswers, this.activeQuestion());
    }

    retrieveQuestionSolution() {
        if (this.questionAttemptedBefore()) {
            let question_id = this.activeQuestion();
            return this.props.optionAnswers[question_id];
        } else {
            return '';
        }

    }

    render() {
        return (
            <div>
                <div className="border pl-2 py-2">
                    <span className="font-bold text-black ">Question No. {this.props.activeQuestion}</span>
                </div>
                <div className="overflow-y-scroll border-b-2 p-2 h-26rem">
                    <div dangerouslySetInnerHTML={{__html: this.props.question.question}}/>
                    {/*Render image of question if available*/}
                    {
                        this.props.question.upload_question !== null ?
                            <img className="img-fluid" src={this.props.question.upload_question} alt=""/> : ''
                    }
                    <div className="flex flex-col pt-2">
                        <label>
                        <span className="pt-px">
                            <input className='question-option' type="radio" name="option"
                                   value="1" onClick={(e) => this.props.handleSelectAnswer(e)}
                                   checked={this.retrieveQuestionSolution() === "1"}/>
                        </span>
                            <span className=" px-2 font-bold"
                                  dangerouslySetInnerHTML={{__html: this.props.question.option1}}/>
                        </label>
                        <label>
                        <span className="pt-px">
                            <input className='question-option' type="radio" name="option"
                                   value="2" onClick={(e) => this.props.handleSelectAnswer(e)}
                                   checked={this.retrieveQuestionSolution() === "2"}/>
                        </span>
                            <span className=" px-2 font-bold"
                                  dangerouslySetInnerHTML={{__html: this.props.question.option2}}/>
                        </label>
                        <label>
                         <span className="pt-px">
                            <input className='question-option' type="radio" name="option"
                                   value="3" onClick={(e) => this.props.handleSelectAnswer(e)}
                                   checked={this.retrieveQuestionSolution() === "3"}/>
                        </span>
                            <span className=" px-2 font-bold"
                                  dangerouslySetInnerHTML={{__html: this.props.question.option3}}/>
                        </label>
                        <label>
                        <span className="pt-px">
                            <input className='question-option' type="radio" name="option"
                                   value="4" onClick={(e) => this.props.handleSelectAnswer(e)}
                                   checked={this.retrieveQuestionSolution() === "4"}/>
                        </span>
                            <span className=" px-2 font-bold"
                                  dangerouslySetInnerHTML={{__html: this.props.question.option4}}/>
                        </label>
                    </div>
                </div>
            </div>
        )
    }
};

const ButtonGroup = (props) => {
    return (
        <div>
            <div className="flex items-center overflow-auto justify-between p-2">
                <button className="bg-blue hover:bg-blue-dark py-2 px-4 rounded" type="button"
                        onClick={props.handleMarkForReview}>
                    <span className="font-bold text-white">Mark for Review {props.next ? '& Next' : ''}</span>
                </button>
                <button className="bg-blue hover:bg-blue-dark mr-auto ml-1 py-2 px-4 rounded" type="button"
                        onClick={props.handleClearResponse}>
                    <span className="font-bold text-white">Clear Response</span>
                </button>
                <button className="inline-block align-baseline bg-blue hover:bg-blue-dark py-2 px-4 rounded"
                        onClick={props.handleSaveAndNext}>
                    <span className="font-bold text-white">Save {props.next ? '& Next' : ''}</span>
                </button>
            </div>
        </div>
    )
};

const RightSide = (props) => {
    return (
        <div>
            <div className="border-grey border-2 h-32">
                <div className="block float-left mx-2 my-4 ">
                    <span className="bg-green py-2 px-3 text-white font-bold rounded"> </span>
                    <span className="font-bold font-sans">Answered</span>
                </div>
                <div className="block float-right mr-5 my-4 ">
                    <span className="bg-red-dark py-2 px-3 text-white font-bold rounded"> </span>
                    <span className="font-bold font-sans">Not Answered</span>
                </div>
                <div className="clear"/>
                <div className="block float-left m-2 h-10 ">
                    <span className="bg-white py-2 px-3 text-black border-black border-2 font-bold rounded"> </span>
                    <span className="font-bold font-sans">Not Visited</span>
                </div>
                <div className="block float-right mt-2 mr-3 h-10">
                    <span className="bg-blue py-2 px-3  text-white font-bold rounded"> </span>
                    <span className="font-bold overflow-auto font-sans">Marked for Review</span>
                </div>
            </div>

            <div className="border-grey border-2 h-64">
                <div className="mb-2 ml-2">
                    <span className="font-sans font-bold text-black">Choose a Question</span>
                </div>
                <div className="container col-md-12 ">
                    <div className="row mr-2 ">
                        {
                            props.questions.items.length > 0 ? props.questions.items.map((x, y) =>
                                <div key={x} className="col-md-2 mb-4">
                                    <button id={`"button-${x}"`}
                                            className={`"bg-white py-2 px-3 text-black border rounded
                                            ${ props.questionState[`"${x}"`] ? props.questionState[`"${x}"`] : ''   }"`}
                                            onClick={(e) => props.handleMakeQuestionActive(e)} value={x}>
                                        {++y}
                                    </button>
                                </div>
                            ) : ''
                        }

                    </div>
                </div>
            </div>
            <div className="flex items-center justify-between mt-2">
                <button
                    className="text-white  border-2 border-blue hover:bg-blue-dark bg-blue py-2 px-4 font-semibold rounded mx-auto"
                    type="button" onClick={props.handleSubmit}>
                    <span className=" font-bold text-xl">Submit</span>
                </button>
            </div>
        </div>
    )
};
const element = document.getElementById('exam');
if (element) {
    ReactDOM.render(<Exam studentName={element.innerText}/>, element);
}

