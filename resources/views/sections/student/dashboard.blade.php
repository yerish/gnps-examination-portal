@extends('layouts.app')

@section('content')
    @include('sections.student.partial.header')

    <div class="col-md-12">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Test Name</th>
                <th scope="col">Class</th>
                <th scope="col">Duration</th>
                <th scope="col"> </th>
            </tr>
            </thead>
            <tbody>
            <input type="hidden" value="{{ $index = 1 }}">
            @foreach($exams as  $exam)
                <tr>
                    <th>{{ $index++ }}</th>
                    <td>{{ $exam->name }}</td>
                    <td>{{ $exam->class }}</td>
                    <td>{{ $exam->duration }}</td>
                    <td>
                        <a href="{{ route('student.exam',$exam->serial) }}" class="btn btn-primary">Attempt</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
