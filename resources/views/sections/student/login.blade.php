@extends('layouts.app')

@section('content')
    <div class="w-full max-w-xs mx-auto mt-5">
        <div class="bg-light text-lg font-bold text-black border shadow rounded py-1 pl-2">{{ __("Student Login Form") }}</div>

        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST"
              action="{{ route('student.login.submit') }}">
            @csrf
            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold" for="username">
                    {{ __('Username') }}
                </label>
                <input class="appearance-none rounded w-full py-2 px-3 text-grey-darker form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                       id="username" type="text" name="username" placeholder="Username"
                       value="{{ old('username') }}"
                       required autofocus>
                @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    {{ __('Password') }}
                </label>
                <input class="appearance-none rounded w-full py-2 px-3 text-grey-darker mb-3 form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                       id="password" type="password" name="password" placeholder="******************" required
                       autofocus>
            </div>
            <div class="flex items-center justify-between">
                <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 mx-auto rounded"
                        type="submit">
                    {{ __(' Sign In') }}
                </button>
            </div>
        </form>
    </div>
@endsection
