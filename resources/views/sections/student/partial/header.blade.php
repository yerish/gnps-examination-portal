<div class="flex items-center overflow-auto justify-between bg-light p-2">
    <span class="capitalize">welcome {{ auth('web')->user()->name }}</span>
    <div class="mx-auto">
        <a href="{{ route('student.dashboard') }}" class="text-red text-xl text-sans font-extrabold hover:text-red hover:no-underline">Student Dashboard</a>
    </div>
    <button class="inline-block align-baseline bg-red hover:bg-red-dark py-2 px-3 rounded"
            href=" {{ route('student.logout') }}"
            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        <span class="text-white font-bold text-sans text-lg">Logout</span>
    </button>
    <form id="logout-form" action="{{ route('student.logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>