@extends('layouts.app')

@section('content')
    @include('sections.admin.partial.header')

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-2 fixed">
                <div class="bg-grey-lightest p-2">
                    <form action="{{ route('admin.class.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="class">Add Class</label>
                            <input type="text" class="form-control" name="class" id="class" required="">
                        </div>
                        <div class="flex justify-center mx-auto">
                            <button type="submit" class="btn btn-outline-primary font-sans w-full">
                                Create Class
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6 offset-3">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Class</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <input type="hidden" value="{{ $index = 1 }}">
                @foreach($classes   as $class)
                    <tr>
                        <td> {{  $index++  }} </td>
                        <td>{{  $class->class  }}</td>
                        <td>
                            <button href="{{ route('admin.class.destroy', $class->id) }}" class="btn btn-outline-danger"
                                    onclick="if(confirm('Are you sure')){
                                            event.preventDefault();
                                            document.getElementById('class-destroy-{{ $class->id }}').submit();
                                            }else{event.preventDefault();}">
                                Delete
                            </button>
                            <form id="class-destroy-{{ $class->id }}" class="hidden" method="post"
                                  action="{{route('admin.class.destroy', $class->id)}}">
                                @method('delete')
                                @csrf
                            </form>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection