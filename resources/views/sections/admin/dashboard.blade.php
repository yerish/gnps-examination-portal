@extends('layouts.app')

@section('content')
    @include('sections.admin.partial.header')
    <div class="container flex items-center overflow-auto justify-between h-50">
        <div href="#" class="btn-group-vertical w-25 mx-auto mt-5 ">
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{route('admin.class')}}">Classes</a>
            {{--<a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"--}}
               {{--href="{{route('admin.event')}}">Events</a>--}}
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{route('admin.teacher')}}">Teacher</a>
        </div>
    </div>
@endsection
