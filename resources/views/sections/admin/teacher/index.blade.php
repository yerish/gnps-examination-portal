@extends('layouts.app')

@section('content')
    @include('sections.admin.partial.header')

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-2 fixed">
                <div class="bg-grey-lightest p-2">
                    <form action="{{ route('admin.teacher.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="teacher">Teacher's Name</label>
                            <input type="text" class="form-control" name="teacher_name" id="teacher" required="">
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username" required="">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" class="form-control" name="password" id="password" required="">
                        </div>

                        <div class="flex justify-center mx-auto">
                            <button type="submit" class="btn btn-outline-primary font-sans w-full">
                                Add Teacher
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6 offset-3">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <input type="hidden" value="{{ $index = 1 }}">
                @foreach($teachers as $teacher)
                    <tr>
                        <td> {{  $index++  }} </td>
                        <td>{{  $teacher->name  }}</td>
                        <td>{{  $teacher->username  }}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                    data-target="#edit-teacher{{ $teacher->id }}">
                                Edit
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="edit-teacher{{ $teacher->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="modelTitleId" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-red">
                                            <h3 class="modal-title text-lg font-bold text-white">Edit Teacher</h3>
                                            <button type="button" class="close text-white" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="w-full max-w-lg mx-auto">
                                            <form class="bg-white rounded px-8 pt-6 pb-8 mb-4"
                                                  action="{{ route('admin.teacher.update',$teacher->id) }}"
                                                  method="post">
                                                @csrf
                                                @method('patch')
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-3 col-form-label text-lg font-bold">Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="name" name="name"
                                                               class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                               value="{{ $teacher->name }}" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="username"
                                                           class="col-md-3 col-form-label text-lg font-bold">Username</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                               id="username" name="username"
                                                               value="{{ $teacher->username }}" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="password" class="col-md-3 col-form-label text-lg font-bold">Password</label>
                                                    <div class="col-md-9">
                                                        <input class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                               id="password" name="password" placeholder="*******" required="">
                                                    </div>
                                                </div>
                                                <div class="flex items-center justify-between">
                                                    <button class="btn-success text-white font-bold text-lg py-2 px-4 mx-auto rounded"
                                                            type="submit">
                                                        Update
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        {{--<td>--}}
                        {{--<button href="{{ route('admin.teacher.destroy', $teacher->id) }}" class="btn btn-outline-danger"--}}
                        {{--onclick="if(confirm('Are you sure')){--}}
                        {{--event.preventDefault();--}}
                        {{--document.getElementById('class-destroy-{{ $teacher->id }}').submit();--}}
                        {{--}else{event.preventDefault();}">--}}
                        {{--Delete--}}
                        {{--</button>--}}
                        {{--<form id="class-destroy-{{ $teacher->id }}" class="hidden" method="post"--}}
                        {{--action="{{route('admin.teacher.destroy', $teacher->id)}}">--}}
                        {{--@method('delete')--}}
                        {{--@csrf--}}
                        {{--</form>--}}
                        {{--</td>--}}
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection