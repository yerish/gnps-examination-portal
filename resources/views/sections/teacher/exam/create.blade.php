@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')

    <div class="container">
        <div class="row">
                <nav class="container pl-5 navbar navbar-expand-lg w-75 bg-white hidden">
                    <div class="collapse navbar-collapse pl-5 ml-5" id="navbarSupportedContent">
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" placeholder="Search" aria-label="Search" type="search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <ul class="navbar-nav mx-auto-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="border-2 border-red text-black font-bold  px-2 py-2">Filter</span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Subject</a>
                                    <a class="dropdown-item" href="#">Level</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Topic</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <form class="mt-4" id="create-exam-form" action="{{ route('exam.store') }}" method="post">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="fixed bg-grey-lightest p-2">
                                <div class="form-group">
                                    <label for="name">Exam Name</label>
                                    <input type="text"
                                           class="form-control" name="name" id="name"  required>
                                </div>
                                <div class="form-group">
                                    <label for="duration">Exam Duration</label>
                                    <input type="text"
                                           class="form-control" name="duration" id="duration" required>
                                </div>

                                <div class="form-group">
                                    <label for="level">Level</label>
                                    <select class="custom-select" name="level" id="level" required>
                                        <option disabled selected>Choose....</option>
                                            <option value="0"> All</option>
                                            <option value="1">Beginner</option>
                                            <option value="2">Medium</option>
                                            <option value="3">Advance</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="class">Class</label>
                                    <select class="custom-select" name="class" id="class" required>
                                        <option disabled selected>Select one</option>
                                        @foreach($classes as $class)
                                            <option value="{{ $class->class }}">Class {{ $class->class }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Exam Description</label>
                                    <textarea class="form-control" name="description" id="description" rows="1" required> </textarea>
                                </div>
                                <div class="flex justify-center mx-auto">
                                    <button type="submit" class="btn btn-warning font-sans w-full">
                                        Create Test
                                    </button>
                                </div>
                            </div>

                            <div class="offset-3 col-9">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Select</th>
                                        <th scope="col">#</th>
                                        <th scope="col">Topic</th>
                                        <th scope="col">Level</th>
                                        <th class="w-screen" scope="col">Questions</th>
                                        <th scope="col"> </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <input type="hidden" value="{{ $index = 1 }}">
                                    @foreach($questions as $question)
                                        <tr>
                                            <th scope="row">
                                                <input name="question[]" value="{{ $question->serial }}" type="checkbox"></th>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $question->topic }}</td>
                                            <td>{{ $question->level }}</td>
                                            <td>{!! __($question->question) !!}</td>
                                            <td>
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-outline-success btn-small" data-toggle="modal"
                                                        data-target="#view-question{{ $question->serial }}">
                                                    View
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="view-question{{ $question->serial }}" tabindex="-1"
                                                     role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <span class="badge badge-primary">Class:- {{ $question->class }}</span>
                                                                <span class="badge badge-warning">{{ $question->subject }}</span>
                                                                <span class="badge badge-success">{{ $question->chapter }}</span>
                                                                <span class="badge badge-light">{{ $question->topic }}</span>
                                                                <span class="badge badge-danger">{{ $question->level }}</span>
                                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {!! __($question->question) !!}
                                                                @if($question->upload_question)
                                                                    <hr>
                                                                    <span class="text-black-50">Uploaded Question</span>
                                                                    <img src=" {{ $question->upload_question }}"
                                                                         alt="">
                                                                @endif
                                                                <hr>
                                                                <span class="text-black-50">Options</span>
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <span class="{{ $question->answer == 1 ? "bg-green text-white": "" }}">{{ $question->option1 }}</span>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <span class="{{ $question->answer == 2 ? "bg-green text-white": "" }}">{{ $question->option2 }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="row border-bottom">
                                                                    <div class="col-6">
                                                                        <span class="{{ $question->answer == 3 ? "bg-green text-white": "" }}">{{ $question->option3 }}</span>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <span class="{{ $question->answer == 4 ? "bg-green text-white": "" }}">{{ $question->option4 }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="">
                                                                    <span class="block text-grey-darker">Solution</span>
                                                                    {!! __($question->solution) !!}
                                                                    @if($question->upload_solution)
                                                                        <hr>
                                                                        <span class="text-black-50">Uploaded Solution</span>
                                                                        <img src=" {{ Storage::url($question->upload_solution) }}"
                                                                             alt="">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
@endsection