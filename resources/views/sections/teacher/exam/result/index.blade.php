@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div class="w-full max-w-xs mx-auto mt-5">
        <div class="bg-light text-lg font-bold text-black border shadow rounded py-1 pl-2">View Students Result</div>
        <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <form action="{{ route('result.filter') }}" method="post">
                @csrf
                {{--<div class="input-group mb-3">--}}
                    {{--<div class="input-group-prepend">--}}
                        {{--<label class="input-group-text" for="class">Select Class</label>--}}
                    {{--</div>--}}
                    {{--<select class="custom-select" id="class" name="class" required>--}}

                        {{--<option selected disabled>Choose...</option>--}}
                        {{--@foreach($classes as $class)--}}
                            {{--<option value="{{ $class->class }}">Class {{ $class->class }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="exam_name">Select Exam</label>
                    </div>
                    <select class="custom-select" id="exam_name" name="exam_name" required>

                        <option selected disabled>Choose...</option>
                        @foreach($exams as $exam)
                            <option value="{{ $exam->serial }}">{{ $exam->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="flex items-center justify-between">
                    <button class="form-group btn-danger text-white font-bold py-2 px-4 mx-auto rounded col-12"
                            type="submit">
                        <span class="text-lg">View</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection