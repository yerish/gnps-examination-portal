@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')

    <div class="container mt-5">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th>Student Name</th>
                <th>Correct</th>
                <th>Incorrect</th>
            </tr>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    <td scope="row">{{ $result->student->name }}</td>
                    <td>{{ $result->correct }}</td>
                    <td>{{ $result->incorrect }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection