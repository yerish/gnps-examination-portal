@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    
    <div class="container flex items-center overflow-auto justify-between h-50">
        <div class="btn-group-vertical w-25 mx-auto mt-5 ">
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('exam.create') }}">Create Exam</a>
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('all.exam') }}">View Exam</a>
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('result.index') }}">Result</a>
        </div>
    </div>
@endsection