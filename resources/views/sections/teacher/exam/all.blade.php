@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div class="container mt-4">
        <div class="row">
            <nav class="container pl-5 navbar navbar-expand-lg w-75 bg-white hidden">
                <div class="collapse navbar-collapse pl-5 ml-5" id="navbarSupportedContent">
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" placeholder="Search" aria-label="Search" type="search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav mx-auto-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="border-2 border-red text-black font-bold  px-2 py-2">Filter</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Subject</a>
                                <a class="dropdown-item" href="#">Level</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Topic</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Test Name</th>
                        <th scope="col">Class</th>
                        <th scope="col">Level</th>
                        <th scope="col"> </th>
                        <th scope="col"> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <input type="hidden" value="{{ $index = 1 }}">
                    @foreach($exams as  $exam)
                        <tr>
                            <th>{{ $index++ }}</th>
                            <td>{{ $exam->name }}</td>
                            <td>{{ $exam->class }}</td>
                            <td>{{ $exam->level }}</td>
                            <td><a href="{{ route('exam.show',$exam->id) }}" class="btn btn-outline-success ">Show</a>
                            </td>
                            <td>
                                <form action="{{ route('exam.destroy',$exam->id) }}"
                                      method="post">
                                    @method('delete')
                                    @csrf
                                    <button type="submit"
                                            class="btn btn-outline-danger">Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection