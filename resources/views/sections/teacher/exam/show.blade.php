@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div class="container mt-4">
        <div class="mx-4">
            <span class="font-bold text-lg col-md-2">Class {{ $exam->class }}</span>
            <span class="font-bold text-lg col-md-2">Total Questions {{ count($questions) }}</span>
            <span class="font-bold text-lg float-right capitalize">{{ $exam->name }}</span>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Topic</th>
                        <th scope="col">Level</th>
                        <th scope="col">Questions</th>
                        <th scope="col"> </th>
                        <th scope="col"> </th>
                    </tr>
                    </thead>
                    <tbody>

                    <input type="hidden" value="{{ $index = 1 }}">
                    @foreach($questions as $question)
                        <tr>
                            <td>{{ $index++ }}</td>
                            <td>{{ $question->topic }}</td>
                            <td>{{ $question->level }}</td>
                            <td>{!! __($question->question) !!}</td>
                            <td>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-success btn-small" data-toggle="modal"
                                        data-target="#view-question{{ $question->serial }}">
                                    View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="view-question{{ $question->serial }}" tabindex="-1"
                                     role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span class="badge badge-primary">Class:- {{ $question->class }}</span>
                                                <span class="badge badge-warning">{{ $question->subject }}</span>
                                                <span class="badge badge-success">{{ $question->chapter }}</span>
                                                <span class="badge badge-light">{{ $question->topic }}</span>
                                                <span class="badge badge-danger">{{ $question->level }}</span>
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! __($question->question) !!}
                                                @if($question->upload_question)
                                                    <hr>
                                                    <span class="text-black-50">Uploaded Question</span>
                                                    <img src=" {{ $question->upload_question }}"
                                                         alt="">
                                                @endif
                                                <hr>
                                                <span class="text-black-50">Options</span>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <span class="{{ $question->answer == 1 ? "bg-green text-white": "" }}">{{ $question->option1 }}</span>
                                                    </div>
                                                    <div class="col-6">
                                                        <span class="{{ $question->answer == 2 ? "bg-green text-white": "" }}">{{ $question->option2 }}</span>
                                                    </div>
                                                </div>
                                                <div class="row border-bottom">
                                                    <div class="col-6">
                                                        <span class="{{ $question->answer == 3 ? "bg-green text-white": "" }}">{{ $question->option3 }}</span>
                                                    </div>
                                                    <div class="col-6">
                                                        <span class="{{ $question->answer == 4 ? "bg-green text-white": "" }}">{{ $question->option4 }}</span>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <span class="block text-grey-darker">Solution</span>
                                                    {!! __($question->solution) !!}
                                                    @if($question->upload_solution)
                                                        <hr>
                                                        <span class="text-black-50">Uploaded Solution</span>
                                                        <img src=" {{ Storage::url($question->upload_solution) }}"
                                                             alt="">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <form action="{{ route('exam.delete.question') }}"
                                      method="post">
                                    @csrf
                                    <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                                    <button type="submit"
                                            class="btn btn-outline-danger">Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection