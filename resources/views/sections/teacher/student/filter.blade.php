@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')

    {{--Student Table TODO ajax / react--}}
    <div class="container m-4">
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Date of Birth</th>
                <th scope="col">Admission No.</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <input type="hidden" value="{{ $index = 1 }}">
            @foreach($students as $student)
                <tr>
                    <th scope="row">{{ $index++ }}</th>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->dob }}</td>
                    <td>{{ $student->admission_number }}</td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-outline-danger btn-small" data-toggle="modal"
                                data-target="#edit-student">
                            Edit
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="edit-student" tabindex="-1" role="dialog"
                             aria-labelledby="modelTitleId"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-red">
                                        <h3 class="modal-title text-lg font-bold text-white">Create Student</h3>
                                        <button type="button" class="close text-white" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="w-full max-w-lg mx-auto">
                                        <form class="bg-white rounded px-8 pt-6 pb-8 mb-4"
                                              action="{{ route('student.update',$student->id) }}" method="post">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="name"
                                                       class="col-md-3 col-form-label text-lg font-bold">Name</label>
                                                <div class="col-md-9">
                                                    <input id="name" name="name"
                                                           class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                           value="{{ $student->name }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="class" class="col-md-3 col-form-label text-lg font-bold">Class</label>
                                                <div class="col-md-9">
                                                    <select class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select "
                                                            id="class" name="class" required>
                                                        <option disabled> Choose Class</option>
                                                        {{--Todo fetch class from database--}}
                                                        @foreach($classes as $class)
                                                            <option value="{{ $class->class}}" {{ $class->class == $student->class ? "selected" : "" }}>
                                                                Class {{ $class->class }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dob"
                                                       class="col-md-3 col-form-label text-lg font-bold">D.O.B</label>
                                                <div class="col-md-9">
                                                    <input class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                           id="dob" name="dob" value="{{ $student->dob }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="admission-number"
                                                       class="col-md-3 col-form-label text-lg font-bold">Adm.
                                                    No</label>
                                                <div class="col-md-9">
                                                    <input id="admission-number"
                                                           class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                                           name="admission_number"
                                                           value="{{ $student->admission_number }}" required>
                                                </div>
                                            </div>
                                            <div class="form-row center">
                                                <div class="offset-3">
                                                    <button class="btn btn-success text-white rounded"
                                                            type="submit">
                                                        Edit
                                                    </button>
                                                </div>
                                                <div class="ml-5">
                                                    <button class="btn btn-danger bg-red text-white rounded"
                                                            href="{{ route('student.destroy',$student->id) }}"
                                                            onclick="if(confirm('Are you sure')){event.preventDefault();
                                                                    document.getElementById('student-destroy-{{ $student->id }}').submit();}else{event.preventDefault();}">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <form id="student-destroy-{{ $student->id }}" class="hidden" method="post"
                                              action="{{ route('student.destroy',  $student->id) }}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{--end Student Table--}}

@endsection