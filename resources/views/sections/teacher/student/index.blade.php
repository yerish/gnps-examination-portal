@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div class="w-full max-w-xs mx-auto mt-5">
        <div class="bg-light text-lg font-bold text-black border shadow rounded py-1 pl-2">View Students</div>
        <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <form action="{{ route('student.class') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="student-class">Select Class</label>
                    </div>
                    <select class="custom-select" id="student-class" name="class" required>

                        <option disabled selected>Choose...</option>
                        @foreach($classes as $class)
                            <option value="{{ $class->class }}">Class {{ $class->class }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="flex items-center justify-between">
                    <button class="form-group btn-danger text-white font-bold py-2 px-4 mx-auto rounded col-12"
                            type="submit">
                        <span class="text-lg">View</span>
                    </button>
                </div>
            </form>
            <div class="flex items-center justify-between">
                <button id="create_new_student_modal_button" data-toggle="modal" data-target="#create_new_student_modal"
                        class="form-group btn btn-outline-primary text-blue hover:text-white font-bold py-2 px-4 mx-auto rounded col-12"
                        type="button">
                    <span class="text-lg">Create new Student</span>
                </button>
            </div>
        </div>
    </div>

    <div id="create_new_student_modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-red">
                    <h3 class="modal-title text-lg font-bold text-white">Create Student</h3>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="w-full max-w-lg mx-auto">
                    <form class="bg-white rounded px-8 pt-6 pb-8 mb-4" action="{{ route('student.store') }}"
                          method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-lg font-bold">Name</label>
                            <div class="col-md-9">
                                <input id="name" name="name"
                                       class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="class" class="col-md-3 col-form-label text-lg font-bold">Class</label>
                            <div class="col-md-9">
                                <select class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select"
                                        id="class" name="class" required>
                                    <option selected disabled> Choose Class</option>
                                    @foreach($classes as $class)
                                        <option value="{{ $class->class }}">Class {{ $class->class }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dob" class="col-md-3 col-form-label text-lg font-bold">D.O.B</label>
                            <div class="col-md-9">
                                <input class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker" id="dob"
                                       name="dob" placeholder="DD/MM/YYYY" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="admission-number" class="col-md-3 col-form-label text-lg font-bold">Adm.
                                No</label>
                            <div class="col-md-9">
                                <input id="admission-number"
                                       class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                       name="admission_number" required/>
                            </div>
                        </div>
                        <div class="flex items-center justify-between">
                            <button class="btn-danger text-white font-bold text-lg py-2 px-4 mx-auto rounded"
                                    type="submit">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection