@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div class="container flex items-center overflow-auto justify-between h-50">
        <div href="#" class="btn-group-vertical w-25 mx-auto mt-5 ">
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('student.index') }}">Student's Section</a>
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('question.index') }}">Question's Section</a>
            <a class="btn btn-danger btn-lg btn-block font-bold text-sans text-white"
               href="{{ route('exam.index') }}">Exam Section</a>
        </div>
    </div>
@endsection
