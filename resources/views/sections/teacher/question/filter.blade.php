@extends('layouts.app')

@section('content')
    @include('sections.teacher.partial.header')
    <div id="view-question"></div>
    <div id="create-question-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="create-question-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal content starts -->
                <div class="modal-header bg-red">
                    <h3 class="modal-title text-lg font-bold text-white">Create Question</h3>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="px-8 pt-6 pb-8" action="{{ route('question.store') }}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-row py-2">
                        <div class="form-group col-md-4">

                            <label class="block text-grey-darker" for="class">Class</label>
                            <select class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select"
                                    id="class" name="class" required>
                                <option disabled selected>Select Class</option>
                                @foreach($classes as $class)
                                    <option value="{{ $class->class }}">Class {{ $class->class }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="subject" class="block text-grey-darker">Subject</label>
                            <select class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select"
                                    id="subject" name="subject" required>
                                <option value="English">English</option>
                                <option value="Maths">Maths</option>
                                <option value="Science">Science</option>
                                <option value="Sst">SSt</option>
                                <option value="Hindi">Hindi</option>
                                <option value="Punjabi">Punjabi</option>
                                <option value="Physics">Physics</option>
                                <option value="Chemistry">Chemistry</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="chapter">Chapter</label>
                            <input type="search"
                                   class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                   id="chapter" name="chapter" placeholder="Chapter" required>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="block text-grey-darker" for="topic">Topic</label>
                            <input id="topic" class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker"
                                   name="topic" placeholder="Topic" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="block text-grey-darker" for="level">Level</label>
                            <select class=" appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select"
                                    id="level" name="level" required>
                                <option value="1">Beginner</option>
                                <option value="2">Medium</option>
                                <option value="3">Advance</option>
                                <option selected value="0">All</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="block text-grey-darker" for="question">Question</label>
                            <textarea class="form-control tinymce" id="question" placeholder="Enter Description Here"
                                      rows="3"
                                      name="question" required> </textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label class="block text-grey-darker" for="uploadQuestion">Upload Question </label>
                            <input type="file" class="form-control-file" id="uploadQuestion" name="upload_question">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="option1">Option 1</label>
                            <input id="option1" class="form-control form-control-sm" placeholder="" type="text"
                                   name="option1" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="option2">Option 2</label>
                            <input id="option2" class="form-control form-control-sm" placeholder="" type="text"
                                   name="option2" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="answer">Answer
                                :</label>
                            <select class="appearance-none border rounded py-2 px-3 w-75 text-grey-darker custom-select"
                                    id="answer" name="answer" required>
                                <option disabled selected>Select..</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="4">Option 4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row ">
                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="option3">Option 3</label>
                            <input id="option3" class="form-control form-control-sm" placeholder="" type="text"
                                   name="option3" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="option4">Option 4</label>
                            <input id="option4" class="form-control form-control-sm" placeholder="" type="text"
                                   name="option4" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="block text-grey-darker" for="solution">Solution</label>
                            <textarea class="form-control tinymce" id="solution" placeholder="Enter Solution Here"
                                      rows="3"
                                      name="solution"> </textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label class="block text-grey-darker" for="uploadSolution">Upload Solution</label>
                            <input type="file" class="form-control-file" id="uploadSolution" name="upload_solution">
                        </div>
                    </div>
                    <div class="flex items-center justify-between">
                        <button class="btn btn-danger bg-red text-white font-bold text-lg py-2 px-4 mx-auto rounded"
                                type="submit">
                            Create
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if($questions)
        {{--Table Questions--}}
        <div class="container">
            <table class="table table-hover">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Added By</th>
                    <th scope="col">Topic</th>
                    <th scope="col">Level</th>
                    <th scope="col">Questions</th>
                    <th scope="col"> </th>
                </tr>
                </thead>
                <tbody>
                <input type="hidden" value="{{ $index = 1 }}">
                @foreach($questions as $question)
                    <tr>
                        <th scope="row">{{ $index++ }}</th>
                        <td class="capitalize">{{ $question->teacher->name }}</td>
                        <td class="capitalize">{{ $question->topic }}</td>
                        <td class="capitalize">{{ $question->level }}</td>
                        <td class="capitalize">{!! __($question->question) !!}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-outline-success btn-small" data-toggle="modal"
                                    data-target="#view-question{{ $question->serial }}">
                                View Questions
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="view-question{{ $question->serial }}" tabindex="-1"
                                 role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <span class="badge badge-primary capitalize">Class:- {{ $question->class }}</span>
                                            <span class="badge badge-warning capitalize">{{ $question->subject }}</span>
                                            <span class="badge badge-success capitalize">{{ $question->chapter }}</span>
                                            <span class="badge badge-light capitalize">{{ $question->topic }}</span>
                                            <span class="badge badge-danger capitalize">{{ $question->level }}</span>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            {!! __($question->question) !!}
                                            @if($question->upload_question)
                                                <hr>
                                                <span class="text-black-50">Uploaded Question</span>
                                                <img src=" {{ $question->upload_question }}" alt="">
                                            @endif
                                            <hr>
                                            <span class="text-black-50">Options</span>
                                            <div class="row">
                                                <div class="col-6">
                                                    <span class="{{ $question->answer == 1 ? "bg-green text-white": "" }}">{{ $question->option1 }}</span>
                                                </div>
                                                <div class="col-6">
                                                    <span class="{{ $question->answer == 2 ? "bg-green text-white": "" }}">{{ $question->option2 }}</span>
                                                </div>
                                            </div>
                                            <div class="row border-bottom">
                                                <div class="col-6">
                                                    <span class="{{ $question->answer == 3 ? "bg-green text-white": "" }}">{{ $question->option3 }}</span>
                                                </div>
                                                <div class="col-6">
                                                    <span class="{{ $question->answer == 4 ? "bg-green text-white": "" }}">{{ $question->option4 }}</span>
                                                </div>
                                            </div>
                                            <div class="">
                                                <span class="block text-grey-darker">Solution</span>
                                                {!! __($question->solution) !!}
                                                @if($question->upload_solution)
                                                    <hr>
                                                    <span class="text-black-50">Uploaded Solution</span>
                                                    <img src=" {{ Storage::url($question->upload_solution) }}"
                                                         alt="">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        {{--End Table Questions--}}
    @endif
@endsection