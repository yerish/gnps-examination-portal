@if(session("errors"))
    <div class="alert alert-danger flash" role="alert">
        @foreach($errors->all() as $error)
            <ul>
                <li>{{ $error }}</li>
            </ul>
        @endforeach
    </div>
@endif